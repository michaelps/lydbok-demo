'use strict'

const http = require('http');
const fs = require('fs');
const Helpers = use('Helpers');

class AppController {

    async index({ request, response, view }) {
        console.log(Helpers.tmpPath());
        return view.render('landing');
        return response.json({success: true, msg: '200OK'});
    }

    async download({ params, response }) {
        const selectedFileType  = params.type ? params.type : 'zip';
        const fileName          = params.book;
        const filePath          = Helpers.publicPath(fileName);

        console.log('Trying to serve file: ' + `${fileName}.${selectedFileType}`);

        if(selectedFileType === 'mp3') {
            console.log('mp3 chosen:', Helpers.tmpPath(`${fileName}.${selectedFileType}`));
            // return response.download(Helpers.tmpPath(`${fileName}.${selectedFileType}`));
        }
        response.header('Content-Disposition', 'attachment');
        response.header('filename', `${fileName}.${selectedFileType}`)
        return response.attachment(
            Helpers.tmpPath(`${fileName}.${selectedFileType}`),
            `lydbok.${selectedFileType}` // custom name
          );
        
    }

}

module.exports = AppController
